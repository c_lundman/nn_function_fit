# What does this code do?

Neural networks (NNs) are used for computing a predicted output (y_pred) given input parameters (X). Given proper training on data with known output (y), the NN can achieve good performance, with y_pred very similar to y. Here, we ask the NN to predict the value of a function that depends on a number of parameters. Specifically, consider a function of x that also depends on other parameters p1, p2, p3, ... We write the function as f(x; p1, p2, p3, ...). Perhaps the function is expensive to evaluate directly, such as a numerically prescribed function? If the function is expensive, one could compute the function on a grid in all parameters (x, p1, p2, p3, ...) and then interpolate between the grid points when the function has to be implemented. Another option is to train a NN on the function, and then use the NN to compute the function value.

The NN method is advantageous when either 1) the interpolation process is more expensive (i.e. takes more time) than the NN evaluation, or 2) the table needed to keep in memory for the interpolation process is larger than the NN. Typically, I think situation 1 is more common.

# Using the code

Here, I've implemented this method in three steps. 1. Given a function and the relevant parameter space, we compute a grid of "data" for the NN to consume. That is, we determine a grid in x, p1, p2, p3, ... and compute the function value y for each grid point. The grid should contain many points for better accuracy, but the trade-off is longer training times. 2. We then specify the NN hyperparameters (e.g. number of hidden layers, nodes per layer, etc.) and train the NN on the data computed from the function. 3. Finally, we can visually evaluate if the NN manages to compute the function well by computing a plot comparing the predicted function vs x for a given set of parameters p1, p2, p3, ... with the true function.

Each step is performed by running a script. However, before running any scripts, the function itself must be defined, and additional parameters such as the parameter grid must be given. The function is given in a file, here called `function.py` as an example, that is located in `./scripts/functions/`. The file can be named anything you like, but the function inside the file must be called `spectrum_function()`. An example function is provided in the file `./scripts/functions/band_function.py`. To check out the code structure, use

    $ tree . -I conda_env

But first, set up your Python environment (i.e. installing the correct libraries). This requires an `anaconda` installation. After installing `anaconda`, do

    $ conda env create -p conda_env -f env.yml

to create your environment, and then do

    $ conda activate

to enter the environment.

The next step is to design your configuration file, which contains additional parameters related to the parameter space and the NN hyperparameters. An example configuration file is found here: `./configs/config.ini`. For a detailed explanation of the `config.ini` file parameters, see `./doc/config_syntax.txt`. The Python scripts take information from the configuration file that is named `config.ini` _and_ is located in the `nn_function_fit/` folder. So, for a given project, first create a function file and place it in `./scripts/functions/`, then copy a configuration file from `./configs/`, put it in the head folder (i.e. `nn_function_fit/`) and modify the configuration file to your liking.

Assuming a function file was created, and the config.ini file is modified to your liking, you can now start running scripts to perform steps 1-3 that was mentioned above. Below are more in-depth explanations of these steps. Always run the scripts from the head folder! If you forget the specific syntax and options for a script, you can always run

    $ python scripts/script_name.py -h

to get a useful help message regarding the script usage.

### 1. Create a data file that contains function information on the parameter space grid

    $ python scripts/nn_make_grid.py -o data/data.csv

will evaluate your function on the grid prescribed in `config.ini`, and save the output to a simple csv (comma-separated values) file, in this example called `data.csv` inside the `./data/` folder. This data file will be used to train the NN.

### 2. Create a NN model from the data file

    $ python scripts/nn_train.py data/data.csv -o models/model.joblib

will train a NN on _each_ of the hyperparameter combinations provided in `config.ini`, check which combination of hyperparameters produced the best fit, pick that set of hyperparameters, print some metrics that describe how well the fit works, and save the NN model as `model.joblib` inside the `./models/` folder. This step can take a long time, depending on the amount of data and how large the NN is.

### 3. Compare NN predictions to your original function

    $ python scripts/nn_compare.py -m models/model.joblib -p A B C ... -o plots/plot.pdf

will produce a plot of the original function with parameters A, B, C etc. Here, A, B, C correspond to values of the parameters p1, p2, p3 discussed above. The function will be plotted against the first parameter, which above is called x. If a model is given (using -m), the script will also plot the NN predictions for the same set of parameter values (A, B, C) and the same set of x-values. The plot will be saved to `plot.pdf` inside the `./plots/` folder if the `-o` option is used; otherwise, the figure will simply be displayed to the screen.

# Quick summary

- Do everything from the head folder: `nn_function_fit`.
- Install `anaconda`.
- Make the Python environment, and activate it: `$ conda env create -p conda_env -f env.yml`, `$ conda activate`.
- Make your own function file and put it inside `./scripts/functions/`.
- The Python scripts use information provided in the file `config.ini` _inside the head folder_.
- Determine the function parameter space inside `config.ini`.
- Run `./scripts/nn_make_grid.py` to make a data set for the NN, based on your function and the provided parameter space.
- Determine the NN hyperparameters inside `config.ini`.
- Run `./scripts/nn_train.py` to train the NN on the data set, making a NN model and printing its performance.
- Run `./scripts/nn_compare.py` to visually inspect the predictions, as compared to the real function.
