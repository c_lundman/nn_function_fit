#!/usr/bin/env python3

import argparse
import configparser
import importlib
import ast
import pandas as pd
import numpy as np
import itertools

def get_args():

    # Default values from config.init
    config = configparser.ConfigParser()
    config.read('config.ini')
    l_default = float(config['General']['LogThreshold'])

    param_spaces = []
    for key in config['FunctionParamSpace']:
        param_spaces.append(ast.literal_eval(config['FunctionParamSpace'][key]))

    #

    spec_module = importlib.import_module(config['General']['FunctionFile'])

    # Get command line arguments
    parser = argparse.ArgumentParser(description='.')
    parser.add_argument('-l', '--log-threshold', type=float, default=l_default,
                        help='the threshold ratio for log-spaced parameter space: '
                        'if max(X)/min(X) > l, logspace will be used (instead of linspace) '
                        '[default=%i]' % (l_default))
    parser.add_argument('output_file', help='the output function grid file (in csv format)')
    args = parser.parse_args()
    return args, spec_module, param_spaces

def get_param_name_and_space(param_spaces, log_threshold):
    param_names = []
    paramss = []
    for i, param_space in enumerate(param_spaces):
        param_names.append("param_%i" % (i))
        if param_space[0] > 0 and param_space[1] > log_threshold*param_space[0]:
            params = np.logspace(np.log10(param_space[0]), np.log10(param_space[1]), param_space[2])
        else:
            params = np.linspace(param_space[0], param_space[1], param_space[2])
        paramss.append(params)
    return param_names, paramss

def main():

    # Get command line arguments
    args, spec_module, param_spaces = get_args()

    # Make the parameter space grid
    param_names, paramss = get_param_name_and_space(param_spaces, args.log_threshold)

    # Evaluate spectrum at each parameter space point
    param_combinations = list(itertools.product(*paramss))
    ys = [spec_module.spectrum_function(*param_combination) for param_combination in param_combinations]

    # Make dataframe
    data = {'y': ys}
    param_combinations_T = list(map(list, zip(*param_combinations)))
    for param_name, params in zip(param_names, param_combinations_T):
        data[param_name] = params
    df = pd.DataFrame(data)

    # Save as csv file
    df.to_csv(args.output_file, index=False)

if __name__ == '__main__':
    main()

