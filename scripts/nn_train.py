#!/usr/bin/env python3

import argparse
import configparser
import importlib
import datetime
import ast
import numpy as np
import pandas as pd
import joblib
from time import time

from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler, FunctionTransformer
from sklearn.compose import TransformedTargetRegressor, ColumnTransformer
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import GridSearchCV

def get_args():

    # Default values from config.init
    config = configparser.ConfigParser()
    config.read('config.ini')
    l_default = float(config['General']['LogThreshold'])
    random_state_default = config['General']['RandomState']
    n_jobs = int(config['General']['NJobs'])
    verbose = ast.literal_eval(config['General']['NJobs'])

    # Get command line arguments
    parser = argparse.ArgumentParser(description='.')
    parser.add_argument('input_file', help='the input data file (in .csv format)')
    parser.add_argument('output_file', help='the output model file (in .joblib format)')
    parser.add_argument('-l', '--log-threshold', type=float, default=l_default, help='the threshold ratio for log-transformations: if max(X)/min(X) > l, variable will be logged [default=%i]' % (l_default))
    parser.add_argument('-rng', '--random-state', default=random_state_default, help='the value passed to all random_state fields. if None, will actually be random [default=%s]' % (random_state_default))
    args = parser.parse_args()

    if args.random_state.lower() == 'none':
        args.random_state = None
    else:
        args.random_state = int(args.random_state)
    args.n_jobs = n_jobs
    args.verbose = verbose

    # Needed parameters
    # if not args.INPUT_FILE:
    #     print('error: need to provide an input file name.')
    #     raise SystemExit
    # if not args.output_file:
    #     print('error: need to provide an output file name.')
    #     raise SystemExit
    return args

def get_data(filename):
    df = pd.read_csv(filename)
    X = df.loc[:, df.columns != 'y']
    Y = df['y']
    return X, Y

def find_lin_and_log_vars(X, log_threshold):
    var_lin = []
    var_log = []
    for label in X.columns:
        if X[label].min() > 0 and X[label].max()/X[label].min() > log_threshold:
            var_log.append(label)
        else:
            var_lin.append(label)
    return var_lin, var_log

def get_param_grid():
    config = configparser.ConfigParser()
    config.read('config.ini')

    param_grid = {}
    pre = 'regressor__mlpregressor__'
    for key in config['HyperParamSpace']:
        param_grid[pre+key] = ast.literal_eval(config['HyperParamSpace'][key])
    return param_grid

def main():

    # Load args and data
    args = get_args()
    X, y = get_data(args.input_file)
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=args.random_state)

    # Define the hyperparameterspace
    param_grid = get_param_grid()

    # Build the model
    var_lin, var_log = find_lin_and_log_vars(X, log_threshold=args.log_threshold)
    preproc_lin = StandardScaler()
    preproc_log = make_pipeline(FunctionTransformer(np.log, validate=False),
                                StandardScaler())
    preprocessor = ColumnTransformer([("preproc_lin", preproc_lin, var_lin),
                                      ("preproc_log", preproc_log, var_log)],
                                      remainder="drop")
    pipe = make_pipeline(preprocessor,
                         MLPRegressor(random_state=args.random_state,
                                      early_stopping=True,
                                      verbose=args.verbose > 3))
    model = TransformedTargetRegressor(regressor=pipe, func=np.log, inverse_func=np.exp)
    search = GridSearchCV(model, param_grid, cv=5, n_jobs=args.n_jobs, verbose=min(3, args.verbose))

    # Do the hyperparameter search and final fit
    t_start = time()
    search.fit(X_train, y_train)
    t_search = time() - t_start

    if args.verbose:
        # Print search results
        print()
        print("Considered hyperparameters:")
        for param in param_grid:
            print("  %s: %s" % (param[25:], param_grid[param]))
        print()

        print("Best fit hyperparameters:")
        for param in param_grid:
            print("  %s: %s" % (param[25:], search.best_params_[param]))
        print()

        # Print metrics
        print("Metrics:")
        print(f"  CV score for best parameters = {search.best_score_:6.4f}")
        print(f"  MLPRegressor R^2 score = {search.score(X_train, y_train):6.4f} (training data)")
        print(f"  MLPRegressor R^2 score = {search.score(X_test, y_test):6.4f} (test data)")
        print(f"  Search time = {str(datetime.timedelta(seconds=int(t_search))):s} [hh:mm:ss]")
        print()

    # Save model
    joblib.dump(search, args.output_file)

if __name__ == '__main__':
    main()
