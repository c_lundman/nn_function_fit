#!/usr/bin/env python3

import argparse
import configparser
import importlib
import ast
import numpy as np
import pandas as pd
import joblib
import matplotlib.pyplot as plt

def get_args():

    # Default values from config.init
    config = configparser.ConfigParser()
    config.read('config.ini')
    spec_module = importlib.import_module(config['General']['FunctionFile'])

    # DIRTY!
    for key in config['FunctionParamSpace']:
        key_0 = key
        break
    x_param_space = ast.literal_eval(config['FunctionParamSpace'][key_0])

    # Get command line arguments
    parser = argparse.ArgumentParser(description='.')
    parser.add_argument('-m', '--model-file', help='the (optional) input model file (in .joblib format)')
    parser.add_argument('-p', '--parameters', nargs='*', type=float, help='the spectrum parameters for the comparison (in the order they appear in the spectrum function)')
    parser.add_argument('-o', '--output-file', help='the output plot file (in .pdf format)')
    args = parser.parse_args()
    return args, spec_module, x_param_space

def main():

    # Load args
    args, spec_module, x_param_space = get_args()

    # Build parameter space
    N = 500
    xs = np.logspace(np.log10(x_param_space[0]), np.log10(x_param_space[1]), N)
    data = {'param_0': xs}
    for i, param in enumerate(args.parameters):
        data['param_%i' % (i+1)] = [param for j in range(N)]
    X = pd.DataFrame(data)

    # Plot real function values at the data point locations
    xs_f = np.logspace(np.log10(x_param_space[0]), np.log10(x_param_space[1]), x_param_space[2])
    data_f = {'param_0': xs_f}
    for i, param in enumerate(args.parameters):
        data_f['param_%i' % (i+1)] = [param for j in range(x_param_space[2])]
    X_f = pd.DataFrame(data_f)
    ys = [spec_module.spectrum_function(*row) for i, row in X_f.iterrows()]
    plt.plot(xs_f, ys, 'k', label=r'$\mathrm{Original \, function \, data}$', marker='.', markersize=3, ls='', lw=1.5)

    # Plot predictions
    if args.model_file:
        model = joblib.load(args.model_file)
        ys_pred = model.predict(X)
        plt.plot(xs, ys_pred, '-k', label=r'$\mathrm{NN \, prediction}$', lw=1)

    # Plot
    plt.style.use('default')
    plt.loglog()
    plt.xlim(min(xs), max(xs))
    plt.ylim(1e-5*max(ys), 1e+1*max(ys))
    plt.xlabel(r'$x$')
    plt.ylabel(r'$f(x; \Theta)$')
    plt.legend(loc='best')
    plt.tight_layout()
    if args.output_file:
        plt.savefig(args.output_file)
    else:
        plt.show()

if __name__ == '__main__':
    main()
