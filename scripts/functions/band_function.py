#!/usr/bin/env python3

import numpy as np

# This file contains the spectrum function.

def spectrum_function(E, E_p, alpha, beta):

    # The Band function

    E_br = (alpha-beta)*E_p/(2. + alpha)
    if E < E_br:
        f = (E**alpha) * np.exp(-(2.+alpha)*E/E_p)
    else:
        f = (E**beta) * np.exp(beta-alpha) * (E_br)**(alpha-beta)
    return f
